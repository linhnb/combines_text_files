from gc import collect
from os import getcwd
from glob import glob
import numpy.random.common
import numpy.random.bounded_integers
import numpy.random.entropy
from pandas import DataFrame, read_csv, concat, ExcelWriter
from time import sleep
import csv

def main():
    cwd = getcwd()
    num_files = 0
    file_list = glob("*.csv")
    if len(file_list) != 0:
        count_list = []
        total = 0
        sample_list = []
        for file_name in file_list:
            num_files = num_files + 1
            full_path = cwd + "\\" + file_name
            # read_file = read_csv(full_path, delimiter='~', encoding='utf-16 BE', header=0, low_memory=False)
            read_file = read_csv(full_path, delimiter='~', encoding='utf-16 BE', header=0, quoting=csv.QUOTE_NONE)
            row_count = read_file.shape[0]
            count_list.append(row_count)
            sample_list.append(read_file.head(5))
            collect()
        count_list_frame = DataFrame({'File_name': file_list, 'data_row_count': count_list})
        sample_list_frame = concat(sample_list, axis=0, ignore_index=True, sort=False)

        for count in count_list:
            total = total + count
        total_result_frame = DataFrame({'Total_files_read': [num_files], 'Total_rows': [total]})
        with ExcelWriter(cwd + '\\Result.xlsx', header=True, encoding='utf-16 BE') as writer:
            total_result_frame.to_excel(writer, sheet_name='Total_results', index=None)
            count_list_frame.to_excel(writer, sheet_name='Count_detail_files', index=None)
            sample_list_frame.to_excel(writer, sheet_name='Sample_data', index=None)
    else:
        print('There is no file to run')
        sleep(2)
        print('bye')


if __name__ == '__main__':
    main()
